package com.rahasak.elastiko

import java.net.InetAddress

import org.elasticsearch.common.settings.Settings
import org.elasticsearch.common.transport.TransportAddress
import org.elasticsearch.transport.client.PreBuiltTransportClient

trait ElasticCluster extends ElasticConf {
  System.setProperty("es.set.netty.runtime.available.processors", "false")

  lazy val settings = Settings.builder()
    .put("cluster.name", elasticCluster)
    .build()
  val client = new PreBuiltTransportClient(settings)
  elasticHosts.foreach(h => client.addTransportAddress(new TransportAddress(InetAddress.getByName(h), elasticPort)))
}

