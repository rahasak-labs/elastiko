package com.rahasak.elastiko

import com.typesafe.config.ConfigFactory

trait ElasticConf {
  val elasticConf = ConfigFactory.load("elastic.conf")

  lazy val elasticCluster = elasticConf.getString("elastic.cluster")

  lazy val documentsElasticIndex = elasticConf.getString("elastic.documents-index")
  lazy val documentsElasticDocType = elasticConf.getString("elastic.documents-doc-type")

  lazy val offersElasticIndex = elasticConf.getString("elastic.offers-index")
  lazy val offersElasticDocType = elasticConf.getString("elastic.offers-doc-type")

  lazy val elasticHosts = elasticConf.getString("elastic.hosts").split(",").toSeq.map(_.trim)
  lazy val elasticPort = elasticConf.getString("elastic.port").toInt
}