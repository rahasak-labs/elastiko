package com.rahasak.elastiko

import java.text.SimpleDateFormat
import java.util
import java.util.{Date, TimeZone}

import org.apache.lucene.search.join.ScoreMode
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.sort.SortOrder

import scala.collection.JavaConverters._

case class Signature(signer: String, status: String, digsig: String, comment: String, timestamp: Date = new Date())

case class Document(id: String, creator: String, name: String, description: String, dept: String, typ: String,
                    blobId: String, signers: List[String], signatures: List[Signature], status: String, timestamp: Date = new Date)

object DocumentStore extends App with ElasticCluster with ElasticConf {

  def toDate(date: String): Date = {
    val sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    sdf.setTimeZone(TimeZone.getTimeZone("UTC"))
    sdf.parse(date)
  }

  def toSigners(obj: Object): List[String] = {
    obj match {
      case strings: util.ArrayList[String] =>
        // multiple elements
        strings.asScala.toList
      case string: String =>
        // single elements
        List(string)
      case _ =>
        List()
    }
  }

  def toSignatures(obj: Object): List[Signature] = {
    obj match {
      case objl: util.ArrayList[util.Map[String, AnyRef]] =>
        // multiple elements
        objl.asScala.map { o =>
          Signature(o.get("signer").asInstanceOf[String],
            o.get("status").asInstanceOf[String],
            o.get("digsig").asInstanceOf[String],
            o.get("comment").asInstanceOf[String],
            toDate(o.get("timestamp").asInstanceOf[String])
          )
        }.toList
      case obj: util.Map[String, AnyRef] =>
        // single elements
        List(Signature(obj.get("signer").asInstanceOf[String],
          obj.get("status").asInstanceOf[String],
          obj.get("digsig").asInstanceOf[String],
          obj.get("comment").asInstanceOf[String],
          toDate(obj.get("timestamp").asInstanceOf[String])
        ))
      case _ =>
        List()
    }
  }

  def toDocument(source: util.Map[String, Object]): Document = {
    Document(
      source.get("id").asInstanceOf[String],
      source.get("creator").asInstanceOf[String],
      source.get("name").asInstanceOf[String],
      source.get("description").asInstanceOf[String],
      source.get("dept").asInstanceOf[String],
      source.get("typ").asInstanceOf[String],
      source.get("blob_id").asInstanceOf[String],
      toSigners(source.get("signers")),
      toSignatures(source.get("signatures")),
      source.get("status").asInstanceOf[String],
      toDate(source.get("timestamp").asInstanceOf[String])
    )
  }

  // match (AND condition)
  val mustBuilder = QueryBuilders.boolQuery()
  mustBuilder.must(QueryBuilders.matchQuery("dept", "dev"))
  mustBuilder.must(QueryBuilders.matchQuery("typ", "inoice"))

  // match (not)
  val mustNotBuilder = QueryBuilders.boolQuery()
  mustNotBuilder.mustNot(QueryBuilders.matchQuery("dept", "dev"))

  // match (OR condition)
  val shouldBuilder = QueryBuilders.boolQuery()
  shouldBuilder.should(QueryBuilders.matchQuery("dept", "dev"))
  shouldBuilder.should(QueryBuilders.matchQuery("typ", "invoice"))

  // terms
  val termBuilder = QueryBuilders.boolQuery()
  termBuilder.must(QueryBuilders.termQuery("signers", "erangaeb@gmail.com"))

  // wildcard
  val wildcardBuilder = QueryBuilders.boolQuery()
  wildcardBuilder.should(QueryBuilders.wildcardQuery("id", "111*"))
  wildcardBuilder.should(QueryBuilders.wildcardQuery("dept", "d*"))

  // nested filters
  val nestedFilters = QueryBuilders.boolQuery()
  nestedFilters.must(QueryBuilders.matchQuery("signatures.signer", "erangaeb@gmail.com"))

  // nested must
  val nestedMustBuilder = QueryBuilders.boolQuery()
  nestedMustBuilder.must(QueryBuilders.nestedQuery("signatures", nestedFilters, ScoreMode.None))

  // nested must not
  val nestedMustNotBuilder = QueryBuilders.boolQuery()
  nestedMustNotBuilder.mustNot(QueryBuilders.nestedQuery("signatures", nestedFilters, ScoreMode.None))

  // range query on timestamp field
  val rangeBuilder = QueryBuilders.boolQuery()
  rangeBuilder.must(
    QueryBuilders.rangeQuery("timestamp")
      .from("2019-06-04T11:00:00")
      .to("2019–08–28T19:00:56")
      .format("yyyy-MM-dd'T'HH:mm:ss")
  )

  // combine multiple builders
  val builder = QueryBuilders.boolQuery()
  builder.must(mustBuilder)
    .must(termBuilder)
    .must(nestedMustBuilder)
    .must(wildcardBuilder)
    .must(rangeBuilder)

  // build search
  val searchBuilder = client
    .prepareSearch(documentsElasticIndex)
    .setTypes(documentsElasticDocType)
    .setQuery(mustBuilder)

  // sort
  searchBuilder.addSort("timestamp", SortOrder.ASC)

  // page
  searchBuilder.setFrom(0).setSize(10)

  // execute search
  val resp = searchBuilder.execute().actionGet()

  // total can be respond with paginated queries
  val total = resp.getHits.totalHits
  println(total)

  // extract objects
  val hits = resp.getHits.getHits
  val docs = hits.filter(h => h.getSourceAsMap != null)
    .map { hit =>
      toDocument(hit.getSourceAsMap)
    }.toList
  println(docs)

}
