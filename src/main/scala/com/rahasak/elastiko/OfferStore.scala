package com.rahasak.elastiko

import java.text.SimpleDateFormat
import java.util
import java.util.{Date, TimeZone}

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods.{GET, PUT}
import akka.http.scaladsl.model.{ContentTypes, HttpRequest, StatusCodes}
import akka.stream.ActorMaterializer
import akka.util.ByteString
import org.elasticsearch.common.unit.DistanceUnit
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.sort.SortOrder

import scala.concurrent.Await
import scala.concurrent.duration._

case class Offer(id: String,
                 company: String,
                 lat: Double,
                 lon: Double,
                 description: String,
                 blob: String,
                 typ: String,
                 timestamp: Date = new Date())

object OfferStore extends App with ElasticCluster {

  implicit val system = ActorSystem.create("elastiko")
  implicit val ec = system.dispatcher
  implicit val materializer = ActorMaterializer()

  def initIndex(): Unit = {

    // params
    val index = "offers"
    val uri = s"http://localhost:9200/$index"
    val json =
      s"""
      {
        "settings":{
          "keyspace": "mystiko"
        },
        "mappings": {
          "offers" : {
            "discover" : "^((?!location).*)",
            "properties": {
              "location": {
                "type": "geo_point",
                "cql_collection": "singleton"
              }
            }
          }
        }
      }
    """

    // check index exists
    implicit val timeout = 40.seconds
    val get = HttpRequest(GET, uri = uri)
    Await.result(Http().singleRequest(get), timeout).status match {
      case StatusCodes.NotFound =>
        // index not exists
        println(s"init index request uri $uri json $json")

        // create index
        val put = HttpRequest(PUT, uri = uri).withEntity(ContentTypes.`application/json`, ByteString(json.stripLineEnd))
        val resp = Await.result(Http().singleRequest(put), timeout)

        println(s"init index response: $resp")
      case _ =>
        // index already exists
        println(s"$index index already exists")
    }
  }

  def toOffer(source: util.Map[String, Object]): Offer = {
    def toDate(date: String): Date = {
      val sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
      sdf.setTimeZone(TimeZone.getTimeZone("UTC"))
      sdf.parse(date)
    }

    def toLatLon(obj: Object): Option[(Double, Double)] = {
      obj match {
        case obj: util.Map[String, AnyRef] =>
          Option(obj.get("lat").asInstanceOf[Double], obj.get("lon").asInstanceOf[Double])
        case _ =>
          None
      }
    }

    val latLon = toLatLon(source.get("location"))
    Offer(
      source.get("id").asInstanceOf[String],
      source.get("company").asInstanceOf[String],
      latLon.get._1,
      latLon.get._2,
      source.get("description").asInstanceOf[String],
      source.get("blob").asInstanceOf[String],
      source.get("typ").asInstanceOf[String],
      toDate(source.get("timestamp").asInstanceOf[String])
    )
  }

  def queryOffers(): Unit = {
    // match (AND condition)
    val matchBuilder = QueryBuilders.boolQuery()
    matchBuilder.must(QueryBuilders.matchQuery("company", "rahasak"))
    matchBuilder.must(QueryBuilders.matchQuery("typ", "dev"))

    // geo distance filter
    // filter location with
    val geoBuilder = QueryBuilders.geoDistanceQuery("location")
      .point(37.9174, -122.3050)
      .distance("10", DistanceUnit.KILOMETERS)

    // combine multiple builders with must(AND condition)
    val queryBuilder = QueryBuilders.boolQuery()
      .must(matchBuilder)
      .must(geoBuilder)

    // build search
    val searchBuilder = client
      .prepareSearch(offersElasticIndex)
      .setTypes(offersElasticDocType)
      .setQuery(queryBuilder)

    // sort
    searchBuilder.addSort("timestamp", SortOrder.ASC)

    // page
    searchBuilder.setFrom(0).setSize(10)

    // execute search
    val resp = searchBuilder.execute().actionGet()

    // total can be respond with paginated queries
    val total = resp.getHits.totalHits
    println(total)

    // extract objects
    val hits = resp.getHits.getHits
    val docs = hits.filter(h => h.getSourceAsMap != null)
      .map { hit =>
        toOffer(hit.getSourceAsMap)
      }.toList
    println(docs)
  }

  // execute
  initIndex()
  queryOffers()

}
